"""
Python script to add a new user to the Report Writer database.
Only necessary to add users that are going to be managers.
Managers are the only users of the Report Writer allowed to
generate reports.

24 September 2015
"""

import sys
import sqlite3 

if len(sys.argv) == 3:
	connection = None

	email = str(sys.argv[1])
	role = str(sys.argv[2])

	if role == "user" or role == "manager":
		try:
			connection = sqlite3.connect("report-writer.db")
			connection_cursor = connection.cursor()
			insert_string = "INSERT OR REPLACE INTO rw_user_role (ID, email, role) VALUES ((SELECT ID FROM rw_user_role WHERE email = \'" + email + "\' AND role = \'" + role + "\'), \'" + email + "\', \'" + role + "\');"
			connection_cursor.execute(insert_string)
			connection.commit()
			print("User entered into the database successfully.")

		except sqlite3.Error as e:
			print("User could not be entered into the database. Error %s:" % e.args[0])
		finally:
			if connection:
				connection.close()
	else:
		print("Please enter a valid role. Enter either user or manager.")
else:
	print("Script to add a user to the Report Writer database.")
	print("Usage: python add-user-report-writer.py <email> <role>")
	print("Where email is the user's email address as used in DSpace and role is either \"manager\" or \"user\".")

