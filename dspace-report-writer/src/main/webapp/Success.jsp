<%--
  Created by: Darryl Meyer
  Date: 15/09/10
  Time: 4:04 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head lang="en">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Report Writer - Success</title>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
</head>
<body role="document">

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}">Report Writer</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <%
                    if (session.getAttribute("user_email") != null) {
                %>
                <li>
                    <a><span class="glyphicon glyphicon-user"></span> Logged&nbsp;in&nbsp;as: ${user_email}</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                </li>
                <%
                } else {
                %>
                <li>
                    <a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-user"></span> Not&nbsp;logged&nbsp;in</a>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>
</nav>

<div class="container">

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">${title}</h3>
        </div>
        <div class="panel-body">
            ${message}
        </div>
    </div>
    <%
        if (session.getAttribute("return_manager") != null) {
    %>
    <a href="${pageContext.request.contextPath}/update">
        <button type="button" class="btn btn-default"> ${return_manager}</button>
    </a>
    <%
        }
    %>
    <a href="${pageContext.request.contextPath}">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Go to home page</button>
    </a>


</div>

<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/docs.min.js"></script>
</body>
</html>