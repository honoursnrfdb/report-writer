<%--
  Created by: Darryl Meyer
  Date: 15/09/18
  Time: 2:09 PM
  Generate new reports using templates on the items in DSpace
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Report Writer - Generate Report</title>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
</head>
<body role="document" onbeforeunload='setDefaultDropDown();'>

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}">Report Writer</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <%
                        if (session.getAttribute("user_email") != null) {
                    %>
                    <a><span class="glyphicon glyphicon-user"></span> Logged&nbsp;in&nbsp;as: ${user_email}</a>
                    <%
                    } else {
                    %>
                    <a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-user"></span> Not&nbsp;logged&nbsp;in</a>
                    <%
                        }
                    %>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <%
        if ((Boolean) session.getAttribute("manager")) {
    %>
    <!-- Main Report Content -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Generate Report From Template</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" id="form-submission-loader" action="${pageContext.request.contextPath}/generate"
                  method="post">
                <p class="help-block">Please select a template for a report to generate from the drop-down menu.</p>
                <div class="form-group" id="select-collection-file-div">
                    <label class="col-md-3" for="select-report">Report Template:</label>

                    <div class="col-md-6">
                        <select name="select-report" class="form-control submission-file-loader" id="select-report">
                            <option value="0" selected="selected">Select...</option>
                            <%
                                if (session.getAttribute("reports") != null) {
                            %>
                            ${reports}
                            <%
                                }
                            %>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-primary pull-left" name="generate"
                               value="Generate Report from Template"
                               tabindex="3"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%
    } else {
    %>
    <!-- User is not logged in -->
    <p>You are not logged in. Please login to submit.</p>
    <a href="${pageContext.request.contextPath}">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Go home</button>
    </a>
    <%
        }
    %>
</div>

<script type="text/javascript" language="javascript" >
    function setDefaultDropDown() {
        document.getElementById("select-report").selectedIndex = -1;
    }
</script>
<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/docs.min.js"></script>
</body>
</html>

