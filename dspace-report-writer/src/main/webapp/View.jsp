<%--
  Created by: Darryl Meyer
  Date: 15/09/08
  Time: 3:54 PM
  View the report generated before downloading it.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Report Writer - View</title>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
</head>
<body role="document">

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}">Report Writer</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <%
                        if (session.getAttribute("user_email") != null) {
                    %>
                    <a><span class="glyphicon glyphicon-user"></span> Logged&nbsp;in&nbsp;as: ${user_email}</a>
                    <%
                    } else {
                    %>
                    <a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-user"></span> Not&nbsp;logged&nbsp;in</a>
                    <%
                        }
                    %>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-out"></span>
                        Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <%
        if (session.getAttribute("user_email") != null) {
    %>
    <a href="${report_download}" target="_blank" download="${report_download}">
        <button type="button" class="btn btn-info"><span class="glyphicon glyphicon-download-alt"></span> Download
            Report
        </button>
    </a>
    <%
        if (session.getAttribute("return_manager") != null) {
    %>
    <a href="${pageContext.request.contextPath}/update">
        <button type="button" class="btn btn-default"> ${return_manager}</button>
    </a>
    <%
        }
    %>
    <a href="${pageContext.request.contextPath}">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Go to home page</button>
    </a>
    <div>
        &nbsp;
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Report generate from template: ${report_name}</h3>
        </div>
        <div class="panel-body">
            <div class="embed-responsive embed-responsive-16by9">
                <object class="embed-responsive-item" type="text/html" data="${report_html}">
                    <p>An error has occurred during the report generation. The content of this report could not be loaded.</p>
                </object>
            </div>
        </div>
    </div>
    <%
    } else {
    %>
    <!-- User is not logged in -->
    <p>You are not logged in. Please login to generate and view reports.</p>
    <a href="${pageContext.request.contextPath}">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Go home</button>
    </a>
    <%
        }
    %>
</div>
<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/docs.min.js"></script>
</body>
</html>
