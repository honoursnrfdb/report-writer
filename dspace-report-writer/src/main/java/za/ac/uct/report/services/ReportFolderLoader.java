package za.ac.uct.report.services;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import za.ac.uct.report.data.Report;
import za.ac.uct.report.storage.DatabaseController;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

/**
 * Darryl Meyer
 * 22 September 2015
 * <p>
 * Class to load the XML specification files in the reports folder and add to the database so that the users can choose from
 * the list of the latest reports.
 */
public class ReportFolderLoader {

    // Path to the folder with the XML specification files
    private String reportFolderPath;

    // Database to add reports to
    private DatabaseController databaseController;

    public ReportFolderLoader(String reportFolderPath, DatabaseController database) {
        logln("Report folder path: " + reportFolderPath);

        this.reportFolderPath = reportFolderPath;
        this.databaseController = database;
    }

    /**
     * Loads the XML specification files in the report folder into the database.
     */
    public void loadReportsIntoDatabase() {

        // Clear old reports from the database
        databaseController.removeReportsTable();
        databaseController.createReportsTable();

        File folder = new File(reportFolderPath);
        File[] files = folder.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isFile() && file.getName().endsWith(".xml")) {
                    try {
                        String content = FileUtils.readFileToString(file);

                        databaseController.insertReport(new Report(getReportName(content), file.getAbsolutePath()));
                    } catch (IOException e) {
                        logErr("Could not load report into the database: " + file.getName() + ", " + e.getMessage());
                    }
                }
            }
        }
    }

    /**
     * Parses the XML specification file to return the name of the report, which is specified in the name attribute of the root
     * report field.
     */
    private String getReportName(String content) {

        String reportName = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(content));

            Document document = builder.parse(is);
            document.getDocumentElement().normalize();

            String nodeName = "";
            NodeList nodeList = document.getElementsByTagName("report");
            for (int i = 0; i < nodeList.getLength(); i++) {

                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    nodeName = node.getNodeName();

                    if (nodeName.equals("report") && ((Element) node).hasAttribute("name")) {
                        reportName = ((Element) node).getAttribute("name");
                    }
                }
            }
        } catch (SAXException | ParserConfigurationException | IOException e) {
            logErr("Could not load XML report file name. " + e.getMessage());
        }

        return reportName;
    }

    private void logln(String str) {

        System.out.println("Report Folder Loader: " + str);
    }

    private void logErr(String str) {

        System.err.println("Report Folder Loader: " + str);
    }
}
