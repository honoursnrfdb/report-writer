package za.ac.uct.report.services;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import za.ac.uct.report.data.HTMLReport;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Darryl Meyer
 * 20 September 2015
 * <p>
 * Factory class for creating reports from an XML specification file.
 */
public class ReportFactory {

    // Path to the XML files.
    private String pathToXML;

    // Path to the root of the webapp
    private String rootServerPath;

    // Path to the generated reports folder.
    private String generatedReportPath = "generated-reports" + File.separator;

    // Contents of the XML specification file.
    private String xmlSpecification;

    // Absolute path to the folder of the generated report
    private String absolutePathToReportFolder;

    // Relative path to the folder of the generated report
    private String relativePathToReportFolder;

    // Date time of the report generation
    private String generationDateTime;

    /**
     * Constucts a new instance of the Report Factory class.
     *
     * @param pathToXML      Path to read the XML file from
     * @param rootServerPath Path to the root of the webapp
     */
    public ReportFactory(String pathToXML, String rootServerPath) {

        this.pathToXML = pathToXML;
        this.rootServerPath = rootServerPath;
        this.xmlSpecification = readContentFromFile(pathToXML);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        Calendar cal = Calendar.getInstance();
        this.generationDateTime = dateFormat.format(cal.getTime());
        String pathToReportFolder = getGeneratedReportFolderName();
        this.relativePathToReportFolder = generatedReportPath + pathToReportFolder;
        this.absolutePathToReportFolder = rootServerPath + File.separator + generatedReportPath + pathToReportFolder;
    }

    /**
     * Generates the HTML report from the XML specification. This method uses the HTMLReport object to build an html report. It
     * also handles the queries to the database and generation of the charts.
     *
     * @return True if report successfully generated, false otherwise.
     */
    public boolean generateReport() {

        boolean reportGenerated;

        HTMLReport htmlReport = new HTMLReport();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xmlSpecification));

            Document document = builder.parse(is);
            document.getDocumentElement().normalize();

            // Loop through all elements in XML file
            String nodeName;
            int titleCount = 0;
            int headingCount = 0;
            int textCount = 0;
            int tableCount = 0;
            int pieChartCount = 0;
            int barChartCount = 0;
            NodeList nodeList = document.getElementsByTagName("*");
            for (int i = 0; i < nodeList.getLength(); i++) {

                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    nodeName = node.getNodeName();

                    // Switch statement to handle the different feilds of the XML specification file.
                    switch (nodeName) {
                        case "title":

                            htmlReport.addTitle(document.getElementsByTagName("title").item(titleCount).getTextContent());
                            titleCount++;
                            break;
                        case "heading":

                            if (((Element) node).hasAttribute("style")) {
                                htmlReport.addHeading(document.getElementsByTagName("heading").item(headingCount)
                                                .getTextContent(),
                                        ((Element) node).getAttribute("style"));
                                headingCount++;
                            } else {
                                htmlReport.addHeading(document.getElementsByTagName("heading").item(headingCount)
                                        .getTextContent());
                                headingCount++;
                            }
                            break;
                        case "text":

                            htmlReport.addText(document.getElementsByTagName("text").item(textCount).getTextContent());
                            textCount++;
                            break;
                        case "table": {

                            DatabaseConnector databaseConnector = new DatabaseConnector();
                            htmlReport.addTable(databaseConnector.getQueryResults(document.getElementsByTagName("table")
                                    .item(tableCount).getTextContent()));
                            tableCount++;
                            break;
                        }
                        case "piechart": {

                            DatabaseConnector databaseConnector = new DatabaseConnector();
                            ChartCreator chartCreator = new ChartCreator();

                            /*
                            Attempts to handle different combinations of the attributes for the pie chart field
                             */
                            if (((Element) node).hasAttribute("charttitle") &&
                                    ((Element) node).hasAttribute("width") &&
                                    ((Element) node).hasAttribute("height")) {
                                try {
                                    htmlReport.addImage(
                                            chartCreator.buildPieChart(absolutePathToReportFolder,
                                                    databaseConnector.getQueryResults(
                                                            document.getElementsByTagName("piechart")
                                                                    .item(pieChartCount).getTextContent()),
                                                    ((Element) node).getAttribute("charttitle"),
                                                    Integer.parseInt(((Element) node).getAttribute("width")),
                                                    Integer.parseInt(((Element) node).getAttribute("height"))));
                                } catch (NumberFormatException e) {
                                    logErr("Pie chart, Cannot parse height or width, falling back to default. " + e.getMessage());
                                    htmlReport.addImage(chartCreator.buildPieChart(absolutePathToReportFolder, databaseConnector
                                            .getQueryResults(document.getElementsByTagName
                                                    ("piechart").item(pieChartCount).getTextContent())));
                                }

                            } else if (((Element) node).hasAttribute("charttitle")) {
                                htmlReport.addImage(chartCreator.buildPieChart(absolutePathToReportFolder, databaseConnector
                                                .getQueryResults(document.getElementsByTagName
                                                        ("piechart").item(pieChartCount).getTextContent()),
                                        ((Element) node).getAttribute("charttitle")));
                            } else {
                                htmlReport.addImage(chartCreator.buildPieChart(absolutePathToReportFolder, databaseConnector
                                        .getQueryResults(document.getElementsByTagName
                                                ("piechart").item(pieChartCount).getTextContent())));
                            }
                            pieChartCount++;
                            break;
                        }
                        case "barchart": {
                            DatabaseConnector databaseConnector = new DatabaseConnector();
                            ChartCreator chartCreator = new ChartCreator();

                            /*
                            Attempts to handle different combinations of the attributes for the bar chart field
                             */
                            if (((Element) node).hasAttribute("charttitle") &&
                                    ((Element) node).hasAttribute("width") &&
                                    ((Element) node).hasAttribute("height") &&
                                    ((Element) node).hasAttribute("xaxis") &&
                                    ((Element) node).hasAttribute("yaxis")) {
                                try {
                                    htmlReport.addImage(chartCreator.buildBarChart(
                                            absolutePathToReportFolder,
                                            databaseConnector.getQueryResults(document.getElementsByTagName
                                                    ("barchart").item(barChartCount).getTextContent()),
                                            ((Element) node).getAttribute("charttitle"),
                                            Integer.parseInt(((Element) node).getAttribute("width")),
                                            Integer.parseInt(((Element) node).getAttribute("height")),
                                            ((Element) node).getAttribute("xaxis"),
                                            ((Element) node).getAttribute("yaxis")));
                                } catch (NumberFormatException e) {
                                    logErr("Bar chart, Cannot parse height or width, falling back to default. " + e.getMessage
                                            ());
                                    htmlReport.addImage(chartCreator.buildBarChart(
                                            absolutePathToReportFolder,
                                            databaseConnector.getQueryResults(document.getElementsByTagName
                                                    ("barchart").item(barChartCount).getTextContent())));
                                }
                            } else if (((Element) node).hasAttribute("charttitle") &&
                                    ((Element) node).hasAttribute("width") &&
                                    ((Element) node).hasAttribute("height")) {
                                try {
                                    htmlReport.addImage(chartCreator.buildBarChart(
                                            absolutePathToReportFolder,
                                            databaseConnector.getQueryResults(document.getElementsByTagName
                                                    ("barchart").item(barChartCount).getTextContent()),
                                            ((Element) node).getAttribute("charttitle"),
                                            Integer.parseInt(((Element) node).getAttribute("width")),
                                            Integer.parseInt(((Element) node).getAttribute("height"))));
                                } catch (NumberFormatException e) {
                                    logErr("Bar chart, Cannot parse height or width, falling back to default. " + e.getMessage
                                            ());
                                    htmlReport.addImage(chartCreator.buildBarChart(
                                            absolutePathToReportFolder,
                                            databaseConnector.getQueryResults(document.getElementsByTagName
                                                    ("barchart").item(barChartCount).getTextContent())));
                                }
                            } else if (((Element) node).hasAttribute("charttitle") &&
                                    ((Element) node).hasAttribute("xaxis") &&
                                    ((Element) node).hasAttribute("yaxis")) {
                                htmlReport.addImage(chartCreator.buildBarChart(
                                        absolutePathToReportFolder,
                                        databaseConnector.getQueryResults(document.getElementsByTagName
                                                ("barchart").item(barChartCount).getTextContent()),
                                        ((Element) node).getAttribute("charttitle"),
                                        0,
                                        0,
                                        ((Element) node).getAttribute("xaxis"),
                                        ((Element) node).getAttribute("yaxis")));
                            } else if (((Element) node).hasAttribute("charttitle")) {
                                htmlReport.addImage(chartCreator.buildBarChart(
                                        absolutePathToReportFolder,
                                        databaseConnector.getQueryResults(document.getElementsByTagName
                                                ("barchart").item(barChartCount).getTextContent()),
                                        ((Element) node).getAttribute("charttitle")));
                            } else {
                                htmlReport.addImage(chartCreator.buildBarChart(
                                        absolutePathToReportFolder,
                                        databaseConnector.getQueryResults(document.getElementsByTagName
                                                ("barchart").item(barChartCount).getTextContent())));
                            }

                            barChartCount++;
                            break;
                        }
                    }
                }
            }
        } catch (SAXException | ParserConfigurationException | IOException e) {
            logErr("An error occurred when attempting to parse the XML specification file. " + e.getMessage());
        }

        reportGenerated = writeContentToFile(getHTMLFileName(), htmlReport.toString());

        reportGenerated = buildDownloadZip(getZipFileName());

        return reportGenerated;
    }

    /**
     * Returns the filename of the generated report with the current date time appended to the name.
     *
     * @return File name of the generated HTML report.
     */
    private String getHTMLFileName() {

        Path path = Paths.get(pathToXML);
        String xmlFilename = path.getFileName().toString();

        return xmlFilename.replace(".xml", "").replace(" ", "-") + "-" + generationDateTime + ".html";
    }

    /**
     * Returns the name of the folder where the generated report html and images will be save to.
     *
     * @return Name of this report's specific folder.
     */
    private String getGeneratedReportFolderName() {

        Path path = Paths.get(pathToXML);
        String xmlFilename = path.getFileName().toString();

        return xmlFilename.replace(".xml", "").replace(" ", "-") + "-" + generationDateTime;
    }

    private String getZipFileName() {

        Path path = Paths.get(pathToXML);
        String xmlFilename = path.getFileName().toString();

        return xmlFilename.replace(".xml", "").replace(" ", "-") + "-" + generationDateTime + ".zip";
    }

    private boolean buildDownloadZip(String filename) {

        logln("absolutePathToReportFolder: " + absolutePathToReportFolder);
        logln("rootServerPath + File.separator + generatedReportPath + filename: " + rootServerPath + File.separator +
                generatedReportPath + filename);

        return packZip(absolutePathToReportFolder, (rootServerPath + File.separator + generatedReportPath + filename));
    }

    /**
     * Packs all the files found in the folderPath into the zip file zipFileName
     *
     * @param folderPath
     * @param zipFilename
     * @return True if zip successful, false otherwise.
     */
    private boolean packZip(String folderPath, String zipFilename) {
        logln("Packing report zip... " + zipFilename);

        boolean zipped = false;

        File folder = new File(folderPath);
        File[] allFiles = folder.listFiles();
        byte[] buffer = new byte[1024];

        if (allFiles != null) {
            try {

                FileOutputStream fileOutputStream = new FileOutputStream(zipFilename);
                ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

                for (File aFile : allFiles) {
                    if (aFile.isFile()) {
                        ZipEntry zipEntry = new ZipEntry(aFile.getName());
                        zipOutputStream.putNextEntry(zipEntry);

                        FileInputStream fileInputStream = new FileInputStream(aFile);

                        int i;
                        while ((i = fileInputStream.read(buffer)) > 0) {
                            zipOutputStream.write(buffer, 0, i);
                        }

                        fileInputStream.close();
                    }
                }
                zipOutputStream.closeEntry();
                zipOutputStream.close();

                zipped = true;
            } catch (Exception e) {
                logErr("An error occurred when creating the download zip. " + e.getMessage());
            }
        } else {
            logErr("No files found to zip.");
        }

        return zipped;
    }

    /**
     * Writes the contents of the generated HTML report to file.
     *
     * @param filename Filename of the generated report.
     * @param content  Content of the report.
     * @return Path to where the report is saved.
     */
    private boolean writeContentToFile(String filename, String content) {

        boolean reportWrittenToFile = false;

        PrintWriter out = null;


        // Try write contents of report
        try {
            String folderToWrite = absolutePathToReportFolder;
            File filePath = new File(folderToWrite);
            File file = new File(folderToWrite + File.separator + filename);

            if (!file.exists()) {
                // Create the path if it does not exist
                filePath.mkdirs();
                // Create the file if it does not exist
                file.createNewFile();
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
            bw.write(content);
            bw.close();

            logln("Content written to file at: " + file.getAbsolutePath());

            reportWrittenToFile = true;

        } catch (IOException e) {
            logErr("Could not write content to file. " + e.getMessage());
        }

        return reportWrittenToFile;
    }

    /*
     * Reads the contents of a file to a string.
     * @param pathToFile Absolute path to file to read.
     * @return String of the contents of the file.
     */
    private String readContentFromFile(String pathToFile) {

        byte[] bytes;
        String fileContents = null;

        try {
            bytes = Files.readAllBytes(Paths.get(pathToFile));
            fileContents = new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            logErr("Could not read data from file. " + e.getMessage());
        }
        return fileContents;
    }


    public String getHTMLFilePath() {

        return relativePathToReportFolder + File.separator + getHTMLFileName();
    }

    public String getZipDownloadPath() {

        return generatedReportPath + getZipFileName();
    }

    public String getAbsolutePathToReportFolder() {

        return absolutePathToReportFolder;
    }

    private void logln(String str) {

        System.out.println("Report Factory: " + str);
    }

    private void logErr(String str) {

        System.err.println("Report Factory: " + str);
    }
}
