package za.ac.uct.report.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Darryl Meyer
 * 21 September 2015
 * <p>
 * Connects to the database of DSpace to preform select queries.
 */
public class DatabaseConnector {

    // Driver to connect to the database
    private String databaseDriver;

    // Database connection username
    private String username;

    // Database connection password
    private String password;

    // Database connection port number
    private String port;

    // Database name
    private String databaseName;

    // URL to connect to the database
    private String url;

    // Connection to database
    private Connection connection;

    public DatabaseConnector() {

        loadConnectionConfig();
        openDatabase();
    }

    /*
     * Load the configuration file to connect to the database.
     */
    private void loadConnectionConfig() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("database");
        databaseDriver = resourceBundle.getString("database.driver");
        databaseName = resourceBundle.getString("database.name");
        username = resourceBundle.getString("database.username");
        password = resourceBundle.getString("database.password");
        url = resourceBundle.getString("database.url");
        port = resourceBundle.getString("database.port");
    }

    /**
     * Opens the connection to the database.
     *
     * @return True if the connection was opened successfully, false otherwise.
     */
    public boolean openDatabase() {

        boolean status = false;

        try {
            // Load the JDBC driver
            Class.forName(databaseDriver);

            // Connect to the database
            if (databaseDriver.equals("org.postgresql.Driver")){
                connection = DriverManager.getConnection("jdbc:postgresql://" + url+ ":" + port + "/" + databaseName,
                        username, password);
                status = true;
            } else {
                logErr("Could not establish connection to DSpace database. Please check the connection settings.");
            }
        } catch (Exception e) {
            logErr("Error opening database: " + e.getMessage());
        }

        return status;
    }

    /**
     * Opens the connection to the database.
     *
     * @return True if the connection was opened successfully, false otherwise.
     */
    public boolean closeDatabase() {

        boolean status = false;

        try {
            connection.close();
            status = true;
        } catch (Exception e) {
            logErr("Error closing database: " + e.getMessage());
        }

        return status;
    }

    /**
     * Checks whether the connection to the database is closed.
     *
     * @return True if connection is closed, false otherwise.
     */
    public boolean isClosed() {

        boolean status = false;

        try {
            status = connection.isClosed();
        } catch (Exception e) {
            logErr("Error reading database closed state: " + e.getMessage());
        }

        return status;
    }

    /**
     * Runs the SQL query against the database and returns the result as an array list of array lists.
     * @param query The SQL query.
     * @return The result of the query.
     */
    public ArrayList<ArrayList<String>> getQueryResults(String query) {
        ArrayList<ArrayList<String>> outerArray = new ArrayList<>();
        ArrayList<String> innerArray;
        Statement statement = null;

        // Check to see if the database connection is open
        if (isClosed()) {
            openDatabase();
        }

        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            int columnCount = resultSet.getMetaData().getColumnCount();

            innerArray = new ArrayList<>();

            for (int i = 1; i <= columnCount; i++) {
                innerArray.add(resultSet.getMetaData().getColumnLabel(i));
            }

            outerArray.add(innerArray);

            while (resultSet.next()) {
                innerArray = new ArrayList<>();

                for (int i = 1; i <= columnCount; i++) {
                    innerArray.add(resultSet.getString(i));
                }

                outerArray.add(innerArray);
            }

        } catch (SQLException e) {
            logErr("Could not parse SQL query statement. " + e.getMessage());
            logErr("SQL query: " + query);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    logErr(e.getMessage());
                }
            }
        }

        return outerArray;
    }

    private void logln(String str) {

        System.out.println("Database Connector: " + str);
    }

    private void logErr(String str) {

        System.err.println("Database Connector: " + str);
    }
}
