package za.ac.uct.report.services;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Darryl Meyer
 * 09 September 2015
 * <p>
 * This class manages operations on user login cookies.
 */
public class CookieManager {

    /**
     * Returns the value of the cookie with the specified name.
     *
     * @param request The HTTP servlet request.
     * @param name    The name of the cookie.
     * @return The value of the cookie.
     */
    public static String getCookieValue(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (name.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    /**
     * Adds a cookie to the HTTP response.
     *
     * @param response The HTTP servlet response.
     * @param name     The name of the cookie.
     * @param value    The value of the cookie.
     * @param maxAge   The maximum age of the cookie.
     */
    public static void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }

    /**
     * Removes the user's login cookie.
     *
     * @param response The HTTP servlet response.
     * @param name     The name of the cookie.
     */
    public static void removeCookie(HttpServletResponse response, String name) {
        Cookie cookie = new Cookie(name, null);
        cookie.setPath("/");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
}
