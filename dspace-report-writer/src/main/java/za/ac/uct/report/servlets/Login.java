package za.ac.uct.report.servlets;

import za.ac.uct.report.services.CookieManager;
import za.ac.uct.report.services.ReportFolderLoader;
import za.ac.uct.report.storage.DatabaseController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Darryl Meyer
 * 09 September 2015
 * <p>
 * Handles user logins for the Report Writer.
 */
@WebServlet(name = "Login")
public class Login extends HttpServlet {

    private String rootServerPath;

    private String pathToDatabase;

    private String reportPath;

    private String port;

    private String hostname;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        loadConnectionConfig();

        String email = request.getParameter("login_email");
        String password = request.getParameter("login_password");

        String token = login(email, password);

        // If the user could not be logged in the token will be null
        if (token != null) {
            // Load database to check the role of the user
            String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes" + File.separator;
            String pathToDatabase = getServletContext().getRealPath(resourcesPath);
            DatabaseController database = new DatabaseController(pathToDatabase);

            rootServerPath = getServletContext().getRealPath("");
            reportPath = rootServerPath + File.separator + "reports" + File.separator;
            ReportFolderLoader reportFolderLoader = new ReportFolderLoader(reportPath, database);
            reportFolderLoader.loadReportsIntoDatabase();

            request.getSession().setAttribute("user_email", email);
            CookieManager.addCookie(response, "rw_", token, 30000);

            // If the user is a manager they are redirected to the approve page, else they are redirected to the submit page
            if (database.isUserManager(email)) {
                // Build the components of the approve page
                String reports = database.getAllReportsHTMLList();
                if (!reports.equals("")) {
                    request.getSession().setAttribute("reports", reports);
                }

                // Set the manager session variable
                request.getSession().setAttribute("manager", true);
                RequestDispatcher dispatcher = request.getRequestDispatcher("/Generate.jsp");
                dispatcher.forward(request, response);
            } else {
                // Only managers are allowed to generate reports
                logErr("Error occurred during login.");
                request.getSession().setAttribute("title", "Login Unsuccessful.");
                request.getSession().setAttribute("message", "You have been logged into the system because you have an " +
                        "account in the DSpace repository but you can not create reports because you do not" +
                        " have the correct permissions to generate reports. Please proceed to logout.");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/Error.jsp");
                dispatcher.include(request, response);
            }
        } else {
            logErr("Error occurred during login.");
            request.getSession().setAttribute("title", "Login Unsuccessful.");
            request.getSession().setAttribute("message", "You could not be logged in. Check that you have entered the correct " +
                    "email and password combination.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Error.jsp");
            dispatcher.include(request, response);
        }

    }

    /*
     * Redirect user if the attempt a GET.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Login.jsp");
        dispatcher.forward(request, response);
    }

    /*
     * Load the configuration file to connect to the rest api.
     */
    public void loadConnectionConfig() {

        ResourceBundle resourceBundle = ResourceBundle.getBundle("rest");
        hostname = resourceBundle.getString("rest.hostname");
        port = resourceBundle.getString("rest.port");
    }


    /**
     * Log the user into the localhost DSpace repository using the REST API.
     * <p>
     * Command line usage for the REST endpoint:  curl -H "Content-Type: application/json" --data '{"email":"email-address",
     * "password":"password"}' http://localhost:8080/rest/login
     *
     * @param email    User email address.
     * @param password User password.
     * @return Token for the user, null if there was an error during login.
     */
    public String login(String email, String password) {

        String serverResponse = null;

        // Try log the user in using the DSpace REST API login endpoint
        try {
            URL url = new URL("http://" + hostname + ":" + port + "/rest/login");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            String input = "{\"email\":\"" + email + "\", \"password\":\"" + password + "\"}";

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != 200) {
                serverResponse = null;
                logErr("Login Failed: HTTP error code: " + conn.getResponseCode());
                logErr("Failed URL: " + url.toString());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                StringBuilder response = new StringBuilder();
                String output;
                while ((output = br.readLine()) != null) {
                    response.append(output);
                }

                logln("Login Successful.");

                serverResponse = response.toString();

                conn.disconnect();
            }

        } catch (IOException e) {
            logErr("Could not log user in. " + e.getMessage());
            e.printStackTrace();
        }

        return serverResponse;
    }

    /*
     * Print to standard out log messages specific to the Login servlet.
     */
    private void logln(String str) {

        System.out.println("Login Servlet: " + str);
    }

    /*
     * Print to standard error out log messages specific to the Login servlet.
     */
    private void logErr(String str) {

        System.err.println("Login Servlet: " + str);
    }
}
