package za.ac.uct.report.servlets;

import za.ac.uct.report.storage.DatabaseController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Darryl Meyer
 * 14 September 2015
 * <p>
 * Updates the session attributes to the latest values before redirecting the user to the correct page
 */
@WebServlet(name = "Update")
public class Update extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    /*
     * Redirects the manager to the correct page and updates any session attributes.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        // Load database to check the role of the user
        String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes" + File.separator;
        String pathToDatabase = getServletContext().getRealPath(resourcesPath);
        DatabaseController database = new DatabaseController(pathToDatabase);

        String email = request.getSession().getAttribute("user_email").toString();

        // Reset session variables
        request.getSession().setAttribute("reports", null);
        request.getSession().setAttribute("return_manager", null);
        request.getSession().setAttribute("report_html", null);
        request.getSession().setAttribute("report_download", null);
        request.getSession().setAttribute("report_name", null);
        request.getSession().setAttribute("logout_success_title", null);
        request.getSession().setAttribute("logout_success_message", null);
        request.getSession().setAttribute("logout_failure_title", null);
        request.getSession().setAttribute("logout_failure_message", null);

        // If the user is a manager they are redirected to the generate page, else they are redirected to the login page
        if (email != null && database.isUserManager(email)) {

            // Build the components of the generate page
            String reports = database.getAllReportsHTMLList();
            if (!reports.equals("")) {
                request.getSession().setAttribute("reports", reports);
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/Generate.jsp");
            dispatcher.forward(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Login.jsp");
            dispatcher.forward(request, response);
        }
    }
}
