package za.ac.uct.report.data;

import java.util.ArrayList;

/**
 * Darryl Meyer
 * 20 September 2015
 * <p>
 * Object class for building a HTML formatted report.
 */
public class HTMLReport {
    // The contents of the report is appended on this string builder object.
    private StringBuilder htmlReportContent = new StringBuilder("");

    // The title of the report.
    private String title;

    public HTMLReport() {

    }

    /**
     * Add a title to the report. Used in the HTML title tag.
     */
    public void addTitle(String title) {

        this.title = title;
    }

    /**
     * Add a paragraph of text to the report.
     */
    public void addText(String text) {

        htmlReportContent.append(HTMLComponents.getParagraphTag(text));
    }

    /**
     * Add a heading to the report with the specified style.
     */
    public void addHeading(String text, String style) {

        htmlReportContent.append(HTMLComponents.getHeadingTag(text, style));
    }

    /**
     * Add a heading to the report. The default style will be used.
     */
    public void addHeading(String text) {

        htmlReportContent.append(HTMLComponents.getHeadingTag(text, "h1"));
    }

    /**
     * Add an image to the report.
     */
    public void addImage(String imageName) {

        htmlReportContent.append(HTMLComponents.getDivTag(HTMLComponents.getImageTag(imageName)));
    }

    /**
     * Add a table to the report.
     */
    public void addTable(ArrayList<ArrayList<String>> tableContent) {

        if (tableContent.size() > 1) {
            htmlReportContent.append(HTMLComponents.getTable(tableContent));
        } else {
            htmlReportContent.append(HTMLComponents.getParagraphTag("(No data)"));
        }
    }

    /**
     * Return the HTML report formatted in the correct HTML 5 structure.
     * doctype - html - head - head - body - body - html.
     */
    public String toString() {

        StringBuilder htmlReportString = new StringBuilder(HTMLComponents.getDoctype());
        htmlReportString.append(HTMLComponents.getHTMLTag(HTMLComponents.getHeadTag(title) + HTMLComponents.getBodyTag
                (htmlReportContent.toString())));

        return htmlReportString.toString();
    }
}
