package za.ac.uct.report.data;

import org.jfree.chart.renderer.category.BarRenderer;

import java.awt.*;

/**
 * Darryl meyer
 * 22 September 2015
 * <p>
 * An extended class of BarRenderer to allow for custom colours of bars in the bar graphs.
 */
public class CustomColourRenderer extends BarRenderer {

    /*
     * The colours to use for the bars in the bar graph.
     */
    private Paint[] colours = new Paint[]{
            Color.red, Color.blue, Color.green,
            Color.yellow, Color.orange, Color.cyan,
            Color.magenta, Color.blue};

    public CustomColourRenderer() {

    }

    /*
     * Returns the colour to use for the specific row and column.
     */
    public Paint getItemPaint(final int row, final int column) {

        return this.colours[column % this.colours.length];
    }
}
