package za.ac.uct.report.data;

/**
 * Darryl Meyer
 * 18 September 2015
 * <p>
 * Object class to hold the data for a report.
 */
public class Report {

    // The report name
    private String name;

    // The path to the XML specifcation file
    private String filePath;

    public Report(String name, String filePath) {

        this.name = name;
        this.filePath = filePath;
    }

    public String getName() {

        return name;
    }

    public String getFilePath() {

        return filePath;
    }

}
