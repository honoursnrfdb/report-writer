package za.ac.uct.report.storage;

import za.ac.uct.report.data.Report;

import java.io.File;
import java.sql.*;

/**
 * Darryl Meyer
 * 11 September 2015
 * <p>
 * Controller class for the Report Writer database.
 */
public class DatabaseController {

    // Name of the database
    final String databaseName = "report-writer.db";
    // Root path of server where the database is located
    private String rootServerPath = "";
    // Connection to database
    private Connection connection;

    /**
     * Creates a new database object at the path passed to it.
     *
     * @param rootServerPath Path to create the database at.
     */
    public DatabaseController(String rootServerPath) {

        logln("File.separator: " + File.separator);

        if (rootServerPath.substring(rootServerPath.length() - 1).equals(File.separator)) {
            this.rootServerPath = rootServerPath;
        } else {
            logln("Appending file separator to the end of path string.");
            this.rootServerPath = rootServerPath + File.separator;
        }

        openDatabase();
    }

    // --- Database control methods ---

    /**
     * Opens the connection to the database.
     *
     * @return True if the connection was opened successfully, false otherwise.
     */
    public boolean openDatabase() {

        boolean status = false;

        try {
            // Load the SQLite JDBC driver
            Class.forName("org.sqlite.JDBC");

            // Connect to the database
            connection = DriverManager.getConnection("jdbc:sqlite:" + rootServerPath + databaseName);

            status = true;
            logln("Database opened successfully. " + "jdbc:sqlite:" + rootServerPath + databaseName);

        } catch (Exception e) {
            logErr("Error opening database: " + e.getMessage());
        }

        return status;
    }

    /**
     * Closes the connection to the database.
     *
     * @return True if the connection was closed successfully, false otherwise.
     */
    public boolean closeDatabase() {

        boolean status = false;

        try {
            connection.close();

            status = true;
            logln("Database closed successfully.");
        } catch (Exception e) {
            logErr("Error closing database: " + e.getMessage());
        }

        return status;
    }

    /**
     * Checks whether the connection to the database is closed.
     *
     * @return True if connection is closed, false otherwise.
     */
    public boolean isClosed() {

        boolean status = false;

        try {
            status = connection.isClosed();
        } catch (Exception e) {
            logErr("Error reading database closed state: " + e.getMessage());
        }

        return status;
    }

    // --- Executing SQL statement ---

    /*
    * Connects to the database and performs the SQL update statement passed to it.
    * @param sql The SQL update statement to perform.
    * @return True if the update was performed successfully.
    */
    public boolean executeSQLUpdate(String sql) {

        boolean queryStatus = false;

        if (isClosed()) {
            openDatabase();
        }

        try {
            // Prepare SQL Update statement
            connection.createStatement().executeUpdate(sql);

            queryStatus = true;
        } catch (Exception e) {
            logErr("Error executing SQL update: " + e.getMessage() + "\nSQL: " + sql);
        }

        return queryStatus;
    }

    // --- Specific function methods ---

    /**
     * Creates the user role table if it does not already exist.
     *
     * @return True if the user role table is crated successfully.
     */
    public boolean createUserRoleTable() {

        boolean status;

        // Create the User Role table
        String sql = "CREATE TABLE IF NOT EXISTS rw_user_role (ID INTEGER PRIMARY KEY, email TEXT NOT NULL, role TEXT NOT " +
                "NULL)";
        status = executeSQLUpdate(sql);

        return status;
    }

    /**
     * Creates the reports table if it does not already exist.
     *
     * @return True if the table was created successfully.
     */
    public boolean createReportsTable() {

        boolean status;

        // Create the User Role table
        String sql = "CREATE TABLE IF NOT EXISTS rw_reports (ID INTEGER PRIMARY KEY, report_name TEXT NOT NULL, file_path TEXT " +
                "NOT NULL)";
        status = executeSQLUpdate(sql);

        return status;
    }

    /**
     * Creates the reports table if it does not already exist.
     *
     * @return True if the table was created successfully.
     */
    public boolean removeReportsTable() {

        boolean status;

        // Create the User Role table
        String sql = "DROP TABLE IF EXISTS rw_reports;";
        status = executeSQLUpdate(sql);

        return status;
    }

    /**
     * Checks to see if the table exists in the database.
     *
     * @param tableName The name of the table to check.
     * @return True if the table exists, false otherwise.
     */
    public boolean tableExists(String tableName) {

        boolean tableExists = false;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;

        try {
            // Prepare SQL Select statement
            preparedStatement = connection.prepareStatement("SELECT name FROM sqlite_master WHERE type='table' AND name = ?;");
            preparedStatement.setString(1, tableName);
            resultSet = preparedStatement.executeQuery();

            String name = resultSet.getString("name");

            resultSet.close();

            if (name.equals(tableName)) {
                tableExists = true;
            }
        } catch (SQLException e) {
            logErr("Could not get table name from database. " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return tableExists;
    }

    /**
     * Inserts a new report into the database.
     *
     * @param report The report object to store in the database.
     * @return True if the report was entered successfully, false otherwise.
     */
    public boolean insertReport(Report report) {

        boolean status = false;

        // Check to see if the database connection is open
        if (isClosed()) {
            openDatabase();
        }

        // Check if table exists
        if (!tableExists("rw_reports")) {
            createReportsTable();
        }

        PreparedStatement preparedStatement = null;
        try {
            // Prepare SQL Update statement
            preparedStatement = connection.prepareStatement("INSERT OR REPLACE INTO rw_reports (ID, report_name, file_path) " +
                    "VALUES ((SELECT ID FROM rw_reports WHERE report_name = ?), ?, ?);");
            preparedStatement.setString(1, report.getName());
            preparedStatement.setString(2, report.getName());
            preparedStatement.setString(3, report.getFilePath());
            preparedStatement.executeUpdate();

            status = true;
        } catch (Exception e) {
            logErr("Error executing SQL update: " + e.getMessage());
        }

        return status;
    }

    /**
     * Returns a report object for the associated row ID.
     *
     * @param rowID The report's unique row ID.
     * @return The report object.
     */
    public Report getReport(String rowID) {

        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        String name = null;
        String filePath = null;

        if (isClosed()) {
            openDatabase();
        }

        try {
            // Prepare SQL Select statement
            preparedStatement = connection.prepareStatement("SELECT report_name, file_path FROM rw_reports WHERE ID = ?;");
            preparedStatement.setString(1, rowID);
            resultSet = preparedStatement.executeQuery();

            // Initialize variables
            name = resultSet.getString("report_name");
            filePath = resultSet.getString("file_path");

            resultSet.close();
        } catch (Exception e) {
            logErr("Error getting report from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return new Report(name, filePath);
    }

    /**
     * Returns a HTML formatted list of the reports.
     *
     * @return HTML formatted list of reports.
     */
    public String getAllReportsHTMLList() {

        String reports = "";

        ResultSet resultSet = null;
        if (isClosed()) {
            openDatabase();
        }

        try {
            // Select SQL statement
            String sql = "SELECT ID, report_name FROM rw_reports;";
            // Prepare SQL Select statement
            resultSet = connection.createStatement().executeQuery(sql);

            // Initialize variables
            String ID = "";
            String name = "";
            String report = "";

            // Loop through all results of the query
            while (resultSet.next()) {
                ID = resultSet.getString("ID");
                name = resultSet.getString("report_name");

                report = "<option value=\"" + ID + "\">" + name + "</option>\n";

                // Add report to report array
                reports += report;
            }

            resultSet.close();
        } catch (Exception e) {
            logErr("Error getting all reports array from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return reports;
    }

    /**
     * Returns the role of the user.
     *
     * @param email The email address for the user.
     * @return The role of the user.
     */
    public String checkUserRole(String email) {

        String role = "user";
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;

        // Check to see if the database connection is open
        if (isClosed()) {
            openDatabase();
        }

        try {
            // Prepare SQL Select statement
            preparedStatement = connection.prepareStatement("SELECT role FROM rw_user_role WHERE email = ?;");
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();

            role = resultSet.getString("role");

            resultSet.close();
        } catch (SQLException e) {
            logErr("Error checking user role. email: " + email + ", " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return role;
    }

    /**
     * Checks to see if the user is a manager.
     *
     * @param email Email address for the user.
     * @return True if the user if a manager, false otherwise.
     */
    public boolean isUserManager(String email) {

        String role = checkUserRole(email);

        return role.equals("manager");
    }


    // --- Logging methods ---

    /*
     * Print to standard out log messages specific to the Database Controller.
     */
    private void logln(String str) {

        System.out.println("Database Controller: " + str);
    }

    /*
     * Print to standard error out log messages specific to the Database Controller.
     */
    private void logErr(String str) {

        System.err.println("Database Controller: " + str);
    }

    /**
     * Prints the working directory of the database to System.out.
     */
    public void pwd() {
        // Check to see if the database connection is open
        if (isClosed()) {
            openDatabase();
        }

        try {
            logln("Database path: " + connection.getMetaData().getURL());
        } catch (Exception e) {
            logErr("Could not print database working directory");
        }
    }
}
