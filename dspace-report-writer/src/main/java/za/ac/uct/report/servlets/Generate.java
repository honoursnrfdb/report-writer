package za.ac.uct.report.servlets;

import za.ac.uct.report.data.Report;
import za.ac.uct.report.services.ReportFactory;
import za.ac.uct.report.storage.DatabaseController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Darryl Meyer
 * 18 September 2015
 * <p>
 * Servlet controlling the selection and generation of reports from the templates.
 */
@WebServlet(name = "Generate")
public class Generate extends HttpServlet {

    // The row ID of the report in the database
    private String reportID = null;

    // The email address of the user logged in
    private String userEmail = null;

    private String generatedReportPath = "generated-reports" + File.separator;

    /*
     * Verifies that the user is allowed to generate reports. If so, coordinates the method calls to generated the report from
     * the selected template.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Performance test start time
        long startTime = System.currentTimeMillis();

        response.setContentType("text/html");

        // If the user entered valid report selection details, then create the report.
        if (setReportDetails(request)) {

            // Get the path to the database
            String rootServerPath = getServletContext().getRealPath("");
            String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes"  + File.separator;
            String pathToDatabase = getServletContext().getRealPath(resourcesPath);
            DatabaseController database = new DatabaseController(pathToDatabase);

            // Load report details from the database
            Report report = database.getReport(reportID);

            // Call to report factory to generate the report
            ReportFactory reportFactory = new ReportFactory(report.getFilePath(), rootServerPath);
            boolean reportGenerated = reportFactory.generateReport();

            // If report generation was successful, redirect to view report page
            if (reportGenerated){
                // Performance test end time
                long endTime = System.currentTimeMillis();

                logln("Performance Test - time to generate report: " + (endTime - startTime) + " ms.");

                request.getSession().setAttribute("return_manager", "Try another report template");
                request.getSession().setAttribute("report_name", report.getName());
                request.getSession().setAttribute("report_html", reportFactory.getHTMLFilePath());
                request.getSession().setAttribute("report_download", reportFactory.getZipDownloadPath());
                response.sendRedirect("/report/View.jsp");
            } else {
                request.getSession().setAttribute("title", "Generation Unsuccessful.");
                request.getSession().setAttribute("message", "Your report could not be generated at this time. Please ensure that " +
                        "you have selected a report template form the drop down list.");
                request.getSession().setAttribute("return_manager", "Try another report template");
                response.sendRedirect("/report/Error.jsp");
            }
        } else {
            request.getSession().setAttribute("title", "Generation Unsuccessful.");
            request.getSession().setAttribute("message", "Your report could not be generated at this time. Please ensure that " +
                    "you have selected a report template form the drop down list.");
            request.getSession().setAttribute("return_manager", "Try another report template");
            response.sendRedirect("/report/Error.jsp");
        }
    }

    /*
     * Sets the variables needed for the report generation process
     */
    private boolean setReportDetails(HttpServletRequest request) {

        boolean detailsSet = false;

        String fileReportsPath = "reports";
        String rootServerPath = getServletContext().getRealPath("");
        String reportsPath = rootServerPath + fileReportsPath;

        File reportsDir = new File(reportsPath);
        if (!reportsDir.exists()) {
            reportsDir.mkdir();
        }

        // Get the selected report from the drop down menu
        try {
            reportID = request.getParameter("select-report");
            if (reportID.equals("0")){
                reportID = null;
            }
        } catch (Exception e) {
            logErr("Error occurred during generation. " + e.getMessage());
        }

        // Get the user's email from the session attribute
        userEmail = request.getSession().getAttribute("user_email").toString();

        if (userEmail != null && reportID != null) {
            detailsSet = true;
        }

        return detailsSet;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/report");
    }

    /*
     * Print to standard out log messages specific to the generate servlet.
     */
    private void logln(String str) {

        System.out.println("Generate Servlet: " + str);
    }

    /*
     * Print to standard error out log messages specific to the generate servlet.
     */
    private void logErr(String str) {

        System.err.println("Generate Servlet: " + str);
    }
}
