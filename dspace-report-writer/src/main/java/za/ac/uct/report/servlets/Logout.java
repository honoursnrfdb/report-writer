package za.ac.uct.report.servlets;

import za.ac.uct.report.services.CookieManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Darryl Meyer
 * 09 September 2015
 * <p>
 * Handles user logouts for the Report Writer.
 */
@WebServlet(name = "Logout")
public class Logout extends HttpServlet {

    private String port;

    private String hostname;

    /*
     * Redirect user if the attempt a POST.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("/Login.jsp");
        dispatcher.forward(request, response);
    }

    /*
     * If the user attempts a GET to this servlet's mapping, it will attempt to log them out.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        loadConnectionConfig();

        String cookieValue = CookieManager.getCookieValue(request, "rw_");

        // Log the user out from DSpace
        boolean loggedOut = logout(cookieValue);
        // Remove the users cookie
        CookieManager.removeCookie(response, "rw_");
        // Set the session variable for the user email to null
        request.getSession().setAttribute("user_email", null);

        // Reset session variables
        request.getSession().setAttribute("reports", null);
        request.getSession().setAttribute("return_manager", null);
        request.getSession().setAttribute("manager", null);

        if (loggedOut) {
            request.getSession().setAttribute("logout_success_title", "Logout Successful.");
            request.getSession().setAttribute("logout_success_message", "You were successfully logged out.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Login.jsp");
            dispatcher.forward(request, response);
        } else {
            request.getSession().setAttribute("logout_failure_title", "Logout Unsuccessful.");
            request.getSession().setAttribute("logout_failure_message", "You could not be logged out successfully.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Login.jsp");
            dispatcher.forward(request, response);
        }
    }

    /*
     * Load the configuration file to connect to the rest api.
     */
    public void loadConnectionConfig() {

        ResourceBundle resourceBundle = ResourceBundle.getBundle("rest");
        hostname = resourceBundle.getString("rest.hostname");
        port = resourceBundle.getString("rest.port");
    }

    /*
     * Log the user out of the localhost DSpace repository using the REST API.
     * <p>
     * Command line usage for the REST endpoint: curl -X POST -H "Content-Type: application/json" -H "rest-dspace-token:
     * token" http://localhost:8080/rest/logout
     *
     * @param token Token of the logged in user.
     * @return True if the user was successfully logged out, false otherwise.
     */
    public boolean logout(String token) {

        boolean loggedOut = false;

        // Try log the user out using the DSpace REST API logout endpoint
        try {

            URL url = new URL("http://" + hostname + ":" + port + "/rest/logout");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("rest-dspace-token", token);

            OutputStream os = conn.getOutputStream();
            os.flush();

            if (conn.getResponseCode() != 200) {
                loggedOut = false;
                logln("Logout Failed: HTTP error code: " + conn.getResponseCode());
                logErr("Failed URL: " + url.toString());
                if (conn.getResponseCode() == 400) {
                    logErr("User token: " + token);
                }

            } else {
                loggedOut = true;
                logln("Logout Successful");
                conn.disconnect();
            }

        } catch (IOException e) {
            logErr("Error occurred during logout. " + e.getMessage());
        }

        return loggedOut;
    }

    /*
     * Print to standard out log messages specific to the Logout servlet.
     */
    private void logln(String str) {

        System.out.println("Logout Servlet: " + str);
    }

    /*
 * Print to standard out log messages specific to the Logout servlet.
 */
    private void logErr(String str) {

        System.err.println("Logout Servlet: " + str);
    }
}
