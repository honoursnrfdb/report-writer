package za.ac.uct.report.data;

import org.apache.commons.lang.WordUtils;

import java.util.ArrayList;

/**
 * Darryl Meyer
 * 20 September 2015
 * <p>
 * This class contains all the components for an HTML page.
 */
public class HTMLComponents {

    /**
     * Contents of a style tag specifying CSS styles.
     */
    public static String style = "table {\n" +
            "\tborder-collapse: collapse;\n" +
            "}\n" +
            "td, th {\n" +
            "\tborder: 1px solid #999;\n" +
            "\tpadding: 0.5rem;\n" +
            "\ttext-align: left;\n" +
            "}" +
            ".chart {\n" +
            "\tdisplay: block;\n" +
            "\tmargin-left: auto;\n" +
            "\tmargin-right: auto;\n" +
            "}";

    /**
     * Returns the doctype tag for the top og the HTML page.
     */
    public static String getDoctype() {

        return "<!doctype html>\n";
    }

    /**
     * Returns the head tag including title and style tags.
     */
    public static String getHeadTag(String title) {

        return "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<title>" + title.trim() + "</title>\n" +
                "\t<style>" + style + "</style>\n" +
                "</head>\n";
    }

    /**
     * Returns the passed string surrounded in html tags
     */
    public static String getHTMLTag(String content) {

        return "<html lang=\"en\">\n" + content + "\n</html>";
    }

    /**
     * Returns the passed string surrounded in body tags
     */
    public static String getBodyTag(String content) {

        return "<body>\n" + content + "</body>\n";
    }

    /**
     * Surrounds the passed string with the passed heading style tag.
     */
    public static String getHeadingTag(String content, String style) {

        return "<" + style + ">" + content.trim() + "</" + style + ">\n";
    }

    /**
     * Returns the passed string surrounded with paragraph tags.
     */
    public static String getParagraphTag(String content) {

        return "<p>" + content.trim() + "</p>\n";
    }

    /**
     * Returns the passed string surrounded with div tags
     */
    public static String getDivTag(String content) {

        return "<div>" + content + "</div>\n";
    }

    /**
     * Takes in an array list of array lists and returns them as an HTML formatted table.
     * The contents of the first inner array list will be used as the table headers.
     */
    public static String getTable(ArrayList<ArrayList<String>> tableContent) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<table>\n" +
                "\t<thead>\n" +
                "\t\t<tr>\n");
        for (int i = 0; i < tableContent.get(0).size(); i++) {
            stringBuilder.append("\t\t\t<th>").append(formatHeading(tableContent.get(0).get(i))).append("</th>\n");
        }
        stringBuilder.append("\t\t</tr>\n" +
                "\t</thead>\n" +
                "\t<tbody>\n");
        for (int i = 1; i < tableContent.size(); i++) {
            stringBuilder.append("\t\t<tr>\n");
            for (int j = 0; j < tableContent.get(i).size(); j++) {
                stringBuilder.append("\t\t\t<td>").append(tableContent.get(i).get(j)).append("</td>\n");
            }
            stringBuilder.append("\t\t</tr>\n");
        }
        stringBuilder.append("\t</tbody>\n" +
                "</table>\n");

        return stringBuilder.toString();
    }

    private static String formatHeading(String heading){
        String formattedHeading = heading.replaceAll("_", " ");
        return WordUtils.capitalizeFully(formattedHeading);
    }

    /**
     * Takes in the name of an image and returns the name of the image in an HTML image tag.
     */
    public static String getImageTag(String imageName){
        return "<img class=\"chart\" src=\"" + imageName + "\">\n";
    }

}
