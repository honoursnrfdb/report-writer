package za.ac.uct.report.services;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.xmlbeans.impl.util.Base64;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import za.ac.uct.report.data.CustomColourRenderer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Darryl Meyer
 * 21 September 2015
 * <p>
 * Class used to create static image charts. Uses the chart generation library JFreeChart.
 */
public class ChartCreator {

    // Path to the generated report folder
    private String generatedReportPath = "generated-reports";

    /**
     * Build a pie chart using the results of an SQL query to a database and use the title supplied and save it at the passed file
     * path.
     *
     * @param pathToSaveImage
     * @param sqlQuery
     * @param chartTitle
     * @return Base64 encoded image of the chart.
     */
    public String buildPieChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery, String chartTitle) {

        return generatePieChart(pathToSaveImage, sqlQuery, chartTitle, 0, 0);
    }

    /**
     * Build a pie chart using the results of an SQL query to a database and save it at the passed file
     * path.
     *
     * @param pathToSaveImage
     * @param sqlQuery
     * @return Base64 encoded image of the chart.
     */
    public String buildPieChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery) {

        return generatePieChart(pathToSaveImage, sqlQuery, null, 0, 0);
    }

    /**
     * Build a pie chart using the results of an SQL query to a database and use the title, width and height supplied and save
     * it at the passed file path.
     *
     * @param pathToSaveImage
     * @param sqlQuery
     * @param chartTitle
     * @param width
     * @param height
     * @return Base64 encoded image of the chart.
     */
    public String buildPieChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery, String chartTitle, int width,
                                int height) {

        return generatePieChart(pathToSaveImage, sqlQuery, chartTitle, width, height);
    }

    /*
     * Generates a static image pie chart using the JFreeReports library.
     */
    private String generatePieChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery, String chartTitle, int width,
                                    int height) {

        PieDataset pieDataset = createPieChartDataset(sqlQuery);

        JFreeChart chart;
        if (chartTitle != null) {
            chart = ChartFactory.createPieChart(chartTitle, pieDataset, true, false, false);
        } else {
            chartTitle = "(not specified)";
            chart = ChartFactory.createPieChart("", pieDataset, true, false, false);
        }

        if (width == 0) {
            width = 480;
        }

        if (height == 0) {
            height = 320;
        }

        File pieChartPath = new File(pathToSaveImage);
        File pieChart = new File(pathToSaveImage + File.separator + "pie-chart-" + getRandomNumberString() + ".png");

        try {
            if (!pieChart.exists()) {
                // Create the path if it does not exist
                pieChartPath.mkdirs();
                // Create the file if it does not exist
                pieChart.createNewFile();
            }
            // Save the chart as a PNG static image
            logln("Creating chart... title: " + chartTitle + ", width: " + width + ", height: " + height);
            logln("Path to image: " + pieChart.getAbsolutePath());

            // Get rid of the labels for the pie segments
            PiePlot piePlot = (PiePlot) chart.getPlot();
            piePlot.setLabelGenerator(new StandardPieSectionLabelGenerator("{1} ({2})"));

            // Save it as a PNG image
            ChartUtilities.saveChartAsPNG(pieChart, chart, width, height);
        } catch (IOException e) {
            logErr("Could not create chart. " + e.getMessage());
        }

        return pieChart.getName();
    }

    /*
     * Parse the array list of array lists crated from querying the database and return the results as a PieChartDataset.
     */
    private PieDataset createPieChartDataset(ArrayList<ArrayList<String>> sqlQuery) {

        DefaultPieDataset defaultPieDataset = new DefaultPieDataset();
        if (sqlQuery.size() > 1) {
            for (int i = 1; i < sqlQuery.size(); i++) {
                ArrayList<String> aSqlQuery = sqlQuery.get(i);

                if (sqlQuery.size() >= 2 && aSqlQuery.get(0) != null && isNumber(aSqlQuery.get(1))) {
                    defaultPieDataset.setValue(aSqlQuery.get(0), new Double(aSqlQuery.get(1)));
                } else {
                    logErr("Could not set pie chart value from row is SQL results.");
                }

            }
        } else {
            logErr("No results received from SQL query.");
        }

        return defaultPieDataset;
    }

    /**
     * Build a bar chart using the results of an SQL query to a database and use the title, width, height, x-axis label, y-axis
     * label supplied and save it at the passed file path.
     *
     * @param pathToSaveImage
     * @param sqlQuery
     * @param chartTitle
     * @param width
     * @param height
     * @param xAxisLabel
     * @param yAxisLabel
     * @return Base64 encoded image of the bar chart.
     */
    public String buildBarChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery, String chartTitle, int width,
                                int height, String xAxisLabel, String yAxisLabel) {

        return generateBarChart(pathToSaveImage, sqlQuery, chartTitle, width, height, xAxisLabel, yAxisLabel);
    }

    /**
     * Build a bar chart using the results of an SQL query to a database and use the title, width, height supplied and save it
     * at the passed file path.
     *
     * @param pathToSaveImage
     * @param sqlQuery
     * @param chartTitle
     * @param width
     * @param height
     * @return Base64 encoded image of the bar chart.
     */
    public String buildBarChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery, String chartTitle, int width,
                                int height) {

        return generateBarChart(pathToSaveImage, sqlQuery, chartTitle, width, height, null, null);
    }

    /**
     * Build a bar chart using the results of an SQL query to a database and use the title  supplied and save it
     * at the passed file path.
     *
     * @param pathToSaveImage
     * @param sqlQuery
     * @param chartTitle
     * @return Base64 encoded image of the bar chart.
     */
    public String buildBarChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery, String chartTitle) {

        return generateBarChart(pathToSaveImage, sqlQuery, chartTitle, 0, 0, null, null);
    }

    /**
     * Build a bar chart using the results of an SQL query to a database and save it
     * at the passed file path.
     *
     * @param pathToSaveImage
     * @param sqlQuery
     * @return Base64 encoded image of the bar chart.
     */
    public String buildBarChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery) {

        return generateBarChart(pathToSaveImage, sqlQuery, null, 0, 0, null, null);
    }

    /*
     * Generates a static image bar chart using the JFreeReports library.
     */
    private String generateBarChart(String pathToSaveImage, ArrayList<ArrayList<String>> sqlQuery, String chartTitle, int width,
                                    int height, String xAxisLabel, String yAxisLabel) {

        DefaultCategoryDataset defaultCategoryDataset = createBarChartDataset(sqlQuery);

        JFreeChart chart;
        if (chartTitle != null && xAxisLabel != null && yAxisLabel != null) {
            chart = ChartFactory.createBarChart(chartTitle, xAxisLabel, yAxisLabel, defaultCategoryDataset,
                    PlotOrientation.VERTICAL, false, false, false);
        } else if (chartTitle != null) {
            xAxisLabel = "(not specified)";
            yAxisLabel = "(not specified)";
            chart = ChartFactory.createBarChart(chartTitle, getXAxisLabel(sqlQuery), getYAxisLabel(sqlQuery),
                    defaultCategoryDataset,
                    PlotOrientation.VERTICAL, false, false, false);
        } else {
            chartTitle = "(not specified)";
            xAxisLabel = "(not specified)";
            yAxisLabel = "(not specified)";
            chart = ChartFactory.createBarChart("", getXAxisLabel(sqlQuery), getYAxisLabel(sqlQuery), defaultCategoryDataset,
                    PlotOrientation.VERTICAL, false, false, false);
        }

        if (width == 0) {
            width = 480;
        }

        if (height == 0) {
            height = 320;
        }
        CategoryPlot plot = chart.getCategoryPlot();

        CategoryItemRenderer renderer = new CustomColourRenderer();
        plot.setRenderer(renderer);

        File barChartPath = new File(pathToSaveImage);
        File barChart = new File(pathToSaveImage + File.separator + "bar-chart-" + getRandomNumberString() + ".png");

        try {
            if (!barChart.exists()) {
                // Create the path if it does not exist
                barChartPath.mkdirs();
                // Create the file if it does not exist
                barChart.createNewFile();
            }
            // Save the chart as a PNG static image
            logln("Creating chart... title: " + chartTitle + ", width: " + width + ", height: " + height + ", x-axis: " +
                    xAxisLabel +
                    ", y-axis: " + yAxisLabel);
            logln("Path to image: " + barChart.getAbsolutePath());

            ChartUtilities.saveChartAsPNG(barChart, chart, width, height);
        } catch (IOException e) {
            logErr("Could not create chart. " + e.getMessage());
        }

        return barChart.getName();
    }

    /**
     * Returns the text value of the first row, first column of the sql query results.
     *
     * @param sqlQuery
     * @return
     */
    private String getXAxisLabel(ArrayList<ArrayList<String>> sqlQuery) {

        String xAxisLabel = "";
        if (sqlQuery.size() > 0) {
            xAxisLabel = sqlQuery.get(0).get(0);
        }
        return formatLabel(xAxisLabel);
    }

    /**
     * Returns the text value of the first row, second column of the sql query results.
     *
     * @param sqlQuery
     * @return
     */
    private String getYAxisLabel(ArrayList<ArrayList<String>> sqlQuery) {

        String yAxisLabel = "";
        if (sqlQuery.size() > 0) {
            yAxisLabel = sqlQuery.get(0).get(1);
        }
        return formatLabel(yAxisLabel);
    }

    private String formatLabel(String label) {

        String formattedLabel = label.replaceAll("_", " ");
        return WordUtils.capitalizeFully(formattedLabel);
    }

    /*
     * Parse the array list of array lists crated from querying the database and return the results as a DefaultCategoryDataset.
     */
    private DefaultCategoryDataset createBarChartDataset(ArrayList<ArrayList<String>> sqlQuery) {

        DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();
        if (sqlQuery.size() > 1) {
            for (int i = 1; i < sqlQuery.size(); i++) {
                ArrayList<String> aSqlQuery = sqlQuery.get(i);

                if (sqlQuery.size() >= 2 && aSqlQuery.get(0) != null && isNumber(aSqlQuery.get(1))) {
                    defaultCategoryDataset.setValue(new Double(aSqlQuery.get(1)), "", aSqlQuery.get(0));
                } else {
                    logErr("Could not set bar chart value from row is SQL results.");
                }

            }
        } else {
            logErr("No results received from SQL query.");
        }

        return defaultCategoryDataset;
    }

    private String getRandomNumberString() {

        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(Integer.MAX_VALUE);
        return randomInt + "";
    }

    /**
     * Method to test if a string is a number.
     */
    private boolean isNumber(String numberString) {

        boolean isNumber = false;
        try {
            double numberDouble = Double.parseDouble(numberString);

            isNumber = true;
        } catch (NumberFormatException e) {
            logErr(e.getMessage());
        }
        return isNumber;
    }

    private void logln(String str) {

        System.out.println("Chart Creator: " + str);
    }

    private void logErr(String str) {

        System.err.println("Chart Creator: " + str);
    }

}
