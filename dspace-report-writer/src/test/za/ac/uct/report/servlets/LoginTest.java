package za.ac.uct.report.servlets;

import org.junit.Test;
import za.ac.uct.report.servlets.Login;

import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

import static org.hamcrest.Matchers.*;

/**
 * Darryl Meyer
 * 23 September 2015
 * <p>
 * Test the Login feature of the DSpace REST API
 */
public class LoginTest {

    @Test
    public void testLogin() throws Exception {
        Login login = new Login();
        login.loadConnectionConfig();
        String loginToken = login.login("myrdar003@myuct.ac.za", "admin");

        Pattern pattern = Pattern.compile("\\w\\w\\w\\w\\w\\w\\w\\w-\\w\\w\\w\\w-\\w\\w\\w\\w-\\w\\w\\w\\w-\\w\\w\\w\\w" +
                "\\w\\w\\w\\w\\w\\w\\w\\w");
        boolean patternMatches = false;
        if (pattern.matcher(loginToken).matches()){
            patternMatches = true;
        } else {
            System.err.println("Pattern does not match login token.");
        }

        assertEquals(true, patternMatches);
    }

    @Test
    public void testLoginLogout() throws Exception {
        Login login = new Login();
        login.loadConnectionConfig();
        Logout logout = new Logout();
        logout.loadConnectionConfig();
        String loginToken = login.login("myrdar003@myuct.ac.za", "admin");
        boolean loggedOut = logout.logout(loginToken);

        assertEquals(true, loggedOut);
    }
}