package za.ac.uct.report.services;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Darryl Meyer
 * 23 September 2015
 * <p>
 * Test connections to the DSpace database
 */
public class DatabaseConnectorTest {

    DatabaseConnector databaseConnector;

    @Before
    public void before() {
        databaseConnector = new DatabaseConnector();
    }
    @After
    public void after() {
        databaseConnector.closeDatabase();
    }


    @Test
    public void testOpenDatabase() throws Exception {

        boolean databaseOpen = databaseConnector.openDatabase();

        assertEquals(true, databaseOpen);
    }

    @Test
    public void testIsClosed() throws Exception {
        boolean isClosed = databaseConnector.isClosed();
        assertEquals(false, isClosed);
    }

    @Test
    public void testGetQueryResultsAllTables() throws Exception {
        String sql = "SELECT tablename FROM pg_catalog.pg_tables WHERE tableowner = 'dspace';";
        ArrayList<ArrayList<String>> outer = new ArrayList<>();
        ArrayList<String> inner = new ArrayList<>();
        inner.add("tablename");
        outer.add(inner);
        inner =  new ArrayList<>();
        inner.add("schema_version");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("fileextension");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("bitstreamformatregistry");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("item2bundle");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("epersongroup2eperson");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("handle");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("workflowitem");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("tasklistitem");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("registrationdata");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("subscription");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("bundle");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("communities2item");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("epersongroup2workspaceitem");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("group2group");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("group2groupcache");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("metadataschemaregistry");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("metadatafieldregistry");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("bitstream");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("most_recent_checksum");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("checksum_results");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("checksum_history");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("workspaceitem");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("collection_item_count");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("community_item_count");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("epersongroup");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("harvested_collection");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("harvested_item");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("collection");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("community2collection");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("community");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("community2community");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("collection2item");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("bundle2bitstream");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("resourcepolicy");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("item");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("versionhistory");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("versionitem");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("eperson");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("doi");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("webapp");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("requestitem");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("metadatavalue");
        outer.add(inner);

        ArrayList<ArrayList<String>> results = databaseConnector.getQueryResults(sql);

        assertEquals(outer, results);
    }

    @Test
    public void testCreateTable() throws Exception {
        String sql = "CREATE TABLE rw_test_table (dummy_data INT);;";
        ArrayList<ArrayList<String>> results = databaseConnector.getQueryResults(sql);
        ArrayList<ArrayList<String>> outer = new ArrayList<>();

        assertEquals(outer, results);
    }

    @Test
    public void testTableExists() throws Exception {
        String sql = "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = " +
                "'rw_test_table');";
        ArrayList<ArrayList<String>> results = databaseConnector.getQueryResults(sql);
        ArrayList<ArrayList<String>> outer = new ArrayList<>();
        ArrayList<String> inner = new ArrayList<>();
        inner.add("exists");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("t");
        outer.add(inner);

        assertEquals(outer, results);
    }

    @Test
    public void testDropTable() throws Exception {
        String sql = "DROP TABLE rw_test_table;";
        ArrayList<ArrayList<String>> results = databaseConnector.getQueryResults(sql);
        ArrayList<ArrayList<String>> outer = new ArrayList<>();

        assertEquals(outer, results);
    }

    @Test
    public void testTableNotExists() throws Exception {
        String sql = "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = " +
                "'rw_test_table');";
        ArrayList<ArrayList<String>> results = databaseConnector.getQueryResults(sql);
        ArrayList<ArrayList<String>> outer = new ArrayList<>();
        ArrayList<String> inner = new ArrayList<>();
        inner.add("exists");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("f");
        outer.add(inner);

        assertEquals(outer, results);
    }
}