package za.ac.uct.report.services;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

/**
 * Darryl Meyer
 * 23 September 2015
 * <p>
 * Test cases for the generating static images of charts.
 */
public class ChartCreatorTest {

    ChartCreator chartCreator;

    @Before
    public void setUp() throws Exception {

        chartCreator = new ChartCreator();
    }

    @Test
    public void testBuildPieChart() throws Exception {

        ClassLoader classLoader = ChartCreatorTest.class.getClassLoader();
        String path = classLoader.getResource("").getPath();
        ArrayList<ArrayList<String>> outer = new ArrayList<>();
        ArrayList<String> inner = new ArrayList<>();
        inner.add("heading 1");
        inner.add("heading 2");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("1");
        inner.add("2");
        outer.add(inner);

        String result = chartCreator.buildPieChart(path, outer, "title", 50, 30);

        Pattern pattern = Pattern.compile("pie-chart-\\d+\\.png");
        boolean patternMatches = false;
        if (pattern.matcher(result).matches()){
            patternMatches = true;
        } else {
            System.err.println("Pattern does not match expected chart output filename.");
        }

        assertEquals(true, patternMatches);
    }

    @Test
    public void testBuildBarChart() throws Exception {

        ClassLoader classLoader = ChartCreatorTest.class.getClassLoader();
        String path = classLoader.getResource("").getPath();
        ArrayList<ArrayList<String>> outer = new ArrayList<>();
        ArrayList<String> inner = new ArrayList<>();
        inner.add("heading 1");
        inner.add("heading 2");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("1");
        inner.add("2");
        outer.add(inner);

        String result = chartCreator.buildBarChart(path, outer, "title", 50, 30, "xaxis", "yaxis");

        Pattern pattern = Pattern.compile("bar-chart-\\d+\\.png");
        boolean patternMatches = false;
        if (pattern.matcher(result).matches()){
            patternMatches = true;
        } else {
            System.err.println("Pattern does not match expected chart output filename.");
        }

        assertEquals(true, patternMatches);
    }
}