package za.ac.uct.report.storage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Darryl Meyer
 * 23 September 2015
 * <p>
 * Test cases for the Report Writer database.
 * <p>
 * This test should be run while the Report Writer is running.
 */
public class DatabaseControllerTest {
    DatabaseController databaseController;

    @Before
    public void before() {

        String path = System.getProperty("user.dir") + File.separator + "target" +
                File.separator + "dspace-report-writer-1.0" + File.separator + "WEB-INF" +
                File.separator + "classes" + File.separator;
        databaseController = new DatabaseController(path);
    }

    @Test
    public void testOpenDatabase() throws Exception {

        boolean opened = databaseController.openDatabase();

        assertTrue(opened);
    }

    @Test
    public void testInsertUserRoleTable() throws Exception {

        boolean tableCreated = databaseController.createUserRoleTable();

        assertTrue(tableCreated);
    }

    @Test
    public void testCheckRole() throws Exception {
        String userRole = databaseController.checkUserRole("myrdar003@myuct.ac.za");

        assertEquals("manager", userRole);
    }

    @Test
    public void testTableExists() throws Exception {

        boolean tableExists = databaseController.tableExists("rw_user_role");

        assertTrue(tableExists);
    }



    @Test
    public void testCloseDatabase() throws Exception {
        boolean databaseClosed = databaseController.closeDatabase();

        assertTrue(databaseClosed);
    }
}