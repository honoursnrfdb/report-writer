package za.ac.uct.report.data;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.ac.uct.report.services.DatabaseConnector;

import static org.junit.Assert.*;

/**
 * Darryl Meyer
 * 23 September 2015
 *
 * Test class for the HTML Report class.
 */
public class HTMLReportTest {

    HTMLReport htmlReport;

    @Before
    public void before() {
        htmlReport = new HTMLReport();
    }

    @Test
    public void testToString() throws Exception {
        htmlReport.addTitle("title");
        htmlReport.addHeading("heading");
        htmlReport.addText("content");
        String htmlReportString = htmlReport.toString();
        String expectedHTML = "<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<title>title</title>\n" +
                "\t<style>table {\n" +
                "\tborder-collapse: collapse;\n" +
                "}\n" +
                "td, th {\n" +
                "\tborder: 1px solid #999;\n" +
                "\tpadding: 0.5rem;\n" +
                "\ttext-align: left;\n" +
                "}.chart {\n" +
                "\tdisplay: block;\n" +
                "\tmargin-left: auto;\n" +
                "\tmargin-right: auto;\n"+
                "}</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>heading</h1>\n" +
                "<p>content</p>\n" +
                "</body>\n" +
                "\n" +
                "</html>";

        assertEquals(expectedHTML, htmlReportString);
    }
}