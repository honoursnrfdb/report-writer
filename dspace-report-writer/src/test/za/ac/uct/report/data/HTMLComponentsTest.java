package za.ac.uct.report.data;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Darryl Meyer
 * 23 September 2015
 * <p>
 * JUnit test class for the HTMLComponents class
 */
public class HTMLComponentsTest {

    @Test
    public void testGetDoctype() throws Exception {

        String doctype = HTMLComponents.getDoctype();
        assertEquals("<!doctype html>\n", doctype);
    }

    @Test
    public void testGetHeadTag() throws Exception {

        String headtag = HTMLComponents.getHeadTag("title");
        String style = HTMLComponents.style;
        assertEquals("<head>\n\t<meta charset=\"utf-8\">\n\t<title>title</title>\n\t<style>" + style + "</style>\n" +
                "</head>\n", headtag);
    }

    @Test
    public void testGetHTMLTag() throws Exception {

        String htmltag = HTMLComponents.getHTMLTag("content");
        assertEquals("<html lang=\"en\">\ncontent\n</html>", htmltag);
    }

    @Test
    public void testGetBodyTag() throws Exception {

        String bodytag = HTMLComponents.getBodyTag("content");
        assertEquals("<body>\ncontent</body>\n", bodytag);
    }

    @Test
    public void testGetHeadingTag() throws Exception {

        String headingtag = HTMLComponents.getHeadingTag("content", "h1");
        assertEquals("<h1>content</h1>\n", headingtag);
    }

    @Test
    public void testGetParagraphTag() throws Exception {

        String paragraphTag = HTMLComponents.getParagraphTag("content");
        assertEquals("<p>content</p>\n", paragraphTag);
    }

    @Test
    public void testGetDivTag() throws Exception {

        String divtag = HTMLComponents.getDivTag("content");
        assertEquals("<div>content</div>\n", divtag);
    }

    @Test
    public void testGetTable() throws Exception {

        ArrayList<ArrayList<String>> outer = new ArrayList<>();
        ArrayList<String> inner = new ArrayList<>();
        inner.add("heading 1");
        inner.add("heading 2");
        outer.add(inner);
        inner = new ArrayList<>();
        inner.add("body 1");
        inner.add("body 2");
        outer.add(inner);

        String table = HTMLComponents.getTable(outer);
        String tableEquals = "<table>\n" +
                "\t<thead>\n" +
                "\t\t<tr>\n" +
                "\t\t\t<th>Heading 1</th>\n" +
                "\t\t\t<th>Heading 2</th>\n" +
                "\t\t</tr>\n" +
                "\t</thead>\n" +
                "\t<tbody>\n" +
                "\t\t<tr>\n" +
                "\t\t\t<td>body 1</td>\n" +
                "\t\t\t<td>body 2</td>\n" +
                "\t\t</tr>\n" +
                "\t</tbody>\n" +
                "</table>\n";
        assertEquals(tableEquals, table);
    }

    @Test
    public void testGetImageBase64Tag() throws Exception {

        String base64Image = HTMLComponents.getImageTag("content");
        assertEquals("<img class=\"chart\" src=\"content\">\n", base64Image);
    }
}